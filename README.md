# Dockerfiles for goto18 summer camp DS track

## Prerequisites

Installed docker:
 - for [mac](https://docs.docker.com/docker-for-mac/install/)
 - for [windows](https://docs.docker.com/docker-for-windows/install/)
 - for [ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [debian](https://docs.docker.com/install/linux/docker-ce/debian/), [centos](https://docs.docker.com/install/linux/docker-ce/centos/), [fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)

## How to

```bash
docker  run --name goto_ds -p 8888:8888 -v /path/to/notebooks/on/your/host:/goto/notebooks --rm droff/goto18-ds-lvl1:v1-py3.6
```
search for token in the output
```bash
 http://<...>:8888/?token=a5c32aaf43071de6ddc3a126b1412a0eb4302ab504bc7949
```
navigate to http://localhost:8888 in your browser and enter the token when prompted

...
```bash
docker stop goto_ds
```