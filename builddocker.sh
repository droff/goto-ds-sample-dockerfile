#!/usr/bin/env bash

DOCKER_USER=droff
VERSION=v1

set -e

docker build -t ${DOCKER_USER}/goto18-ds-lvl1:${VERSION}-py3.6 -f ./ds-level1/Dockerfile-py3.6 .
docker push ${DOCKER_USER}/goto18-ds-lvl1:${VERSION}-py3.6